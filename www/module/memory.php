<?php
class kmemoryclass {
	var $class;
	var $speed;
	static function save($saveid=-1){
		$savepc = new kmemoryclass;
		if ($saveid != -1) {$savepc->load($saveid);}
		$savepc->class = ($_REQUEST['class'] != -1) ? ($_REQUEST['class']) : (0);
		//$savepc->class = $_REQUEST['class'];
		$savepc->speed = $_REQUEST['speed'];
		
		if ($saveid != -1){
			$savepc->update();
		} else {
			$savepc->add();
		}

		return $savepc->class;	
	}

	static function delete ($id) {
		$kd = new kdb;
		$kd->query("DELETE FROM `k_memoryclass` WHERE `class`='$id'");
		$kd->query("UPDATE `k_memory` SET `class`='' WHERE `class`='$id'");
		unset($kd);
	}

	function add(){
		$kd = new kdb;
		$kd->query("INSERT INTO `k_memoryclass` (
			`speed`
		) VALUES (
			'$this->speed'
		)
			");
		$this->id = $kd->getlastkey();
		unset($kd);
	}

	function update(){
		$kd = new kdb;
		$kd->query("UPDATE `k_memoryclass`
		SET
			`speed` = '$this->speed'
		WHERE
			`class` = '$this->class'
		");
		unset($kd);
		
	}

	function isexist($id){
		$result = false;
		$kd = new kdb;
		$kd->query("SELECT `speed` FROM `k_memoryclass` WHERE `class`='$id'");
		$result = ($u0=$kd->read());
		unset($kd);
		return $result;
	}

	function load($id){
		$kd = new kdb;
		$kd->query("SELECT 
			`class`,
			`speed`
			FROM `k_memoryclass`
				WHERE `class`=$id");
		if ($u0 = $kd->read()){
			$this->class = $u0[0];
			$this->speed = $u0[1];
		}
	}

	static function getall(){
		$kd = new kdb;
		$kd->query("SELECT 
						n0.class,
						n0.speed
					FROM `k_memoryclass` as n0
					ORDER BY n0.class ASC");
		$objs = array();
		while ($u0 = $kd->read()){
			$obj['class'] = $u0[0];
			$obj['speed'] = $u0[1];
			$objs[] = $obj;
		}
		unset($kd);
		//die(print_r($objs));
		return $objs;
	}
}


class kmemorytype {
	var $memorytypeid;
	var $type;
	static function save($saveid=-1){
		$savepc = new kmemorytype;
		if ($saveid != -1) {$savepc->load($saveid);}
		$savepc->memorytypeid = ($_REQUEST['memorytypeid'] != -1) ? ($_REQUEST['memorytypeid']) : (0);
		//$savepc->id = $_REQUEST['id'];
		$savepc->type = $_REQUEST['type'];
		
		if ($saveid != -1){
			$savepc->update();
		} else {
			$savepc->add();
		}

		return $savepc->memorytypeid;	
	}

	static function delete ($id) {
		$kd = new kdb;
		$kd->query("DELETE FROM `k_memorytype` WHERE `memorytypeid`='$id'");
		$kd->query("UPDATE `k_memory` SET `type`='' WHERE `type`='$id'");
		$kd->query("UPDATE `k_phototech` SET `memorytype`='' WHERE `memorytype`='$id'");
		unset($kd);
	}

	function add(){
		$kd = new kdb;
		$kd->query("INSERT INTO `k_memorytype` (
			`type`
		) VALUES (
			'$this->type'
		)
			");
		$this->id = $kd->getlastkey();
		unset($kd);
	}

	function update(){
		$kd = new kdb;
		$kd->query("UPDATE `k_memorytype`
		SET
			`type` = '$this->type'
		WHERE
			`memorytypeid` = '$this->memorytypeid'
		");
		unset($kd);
		
	}

	function isexist($id){
		$result = false;
		$kd = new kdb;
		$kd->query("SELECT `id` FROM `k_memorytype` WHERE `memorytypeid`='$id'");
		$result = ($u0=$kd->read());
		unset($kd);
		return $result;
	}

	function load($id){
		$kd = new kdb;
		$kd->query("SELECT 
			`memorytypeid`,
			`type`
			FROM `k_memorytype`
				WHERE `memorytypeid`=$id");
		if ($u0 = $kd->read()){
			$this->memorytypeid = $u0[0];
			$this->type = $u0[1];
		}
	}
	static function getall(){
		$kd = new kdb;
		$kd->query("SELECT 
						n0.memorytypeid,
						n0.type
					FROM `k_memorytype` as n0
					ORDER BY n0.memorytypeid ASC");
		$objs = array();
		while ($u0 = $kd->read()){
			$obj['memorytypeid'] = $u0[0];
			$obj['type'] = $u0[1];
			$objs[] = $obj;
		}
		unset($kd);
		return $objs;
	}		
}


class kmemory {
	var $serial_number;
	var $manufacturer;
	var $type;
	var $memory;
	var $class;

	static function save($saveid=-1){
		$savepc = new kmemory;
		if ($saveid != -1) {$savepc->load($saveid);}

		$savepc->serial_number = ($_REQUEST['serial_number'] != -1) ? ($_REQUEST['serial_number']) : (0);
		$savepc->manufacturer = $_REQUEST['manufacturer'];
		$savepc->type = new kmemorytype();
		$savepc->type->load($_REQUEST['type']);

		$savepc->memory = $_REQUEST['memory'];
		$savepc->class = new kmemoryclass();
		$savepc->class->load($_REQUEST['class']);
		
		if ($saveid != -1){
			$savepc->update();
		} else {
			$savepc->add();
		}

		return $savepc->id;	
	}

	static function delete ($id) {
		$kd = new kdb;
		$kd->query("DELETE FROM `k_memory` WHERE `serial_number`='$id'");
		unset($kd);
	}

	function add(){
		$kd = new kdb;
		$type = $this->type->id;
		$class = $this->class->class;
		$kd->query("INSERT INTO `k_memory` (
			`manufacturer`,
			`type`,
			`memory`,
			`class`
		) VALUES (
			'$this->manufacturer',
			'$type',
			'$this->memory',
			'$class'
		)
			");
		$this->id = $kd->getlastkey();
		unset($kd);
	}

	function update(){
		$kd = new kdb;
		$type = $this->type->id;
		$class = $this->class->class;
		$kd->query("UPDATE `k_memory`
		SET
			`manufacturer` = '$this->manufacturer',
			`type` = '$type',
			`memory` = '$this->memory',
			`class` = '$class'
		WHERE
			`serial_number` = '$this->serial_number'
		");
		unset($kd);
		
	}

	function isexist($id){
		$result = false;
		$kd = new kdb;
		$kd->query("SELECT `serial_number` FROM `k_memory` WHERE `serial_number`='$id'");
		$result = ($u0=$kd->read());
		unset($kd);
		return $result;
	}

	function load($id){
		//echo 'memory load <br>';
		$kd = new kdb;
		$kd->query("SELECT 
			`serial_number`,
			`manufacturer`,
			`type`,
			`memory`,
			`class`
			FROM `k_memory`
				WHERE `serial_number`=$id");
		if ($u0 = $kd->read()){
			$this->serial_number = $u0[0];
			$this->manufacturer = $u0[1];
			$this->type = new kmemorytype();
			$this->type->load($u0[2]);
			$this->memory = $u0[3];
			$this->class = new kmemoryclass();
			$this->class->load($u0[4]);

		}
	}

	static function getall(){
		$kd = new kdb;
		$kd->query("SELECT 
						n0.serial_number,
						n0.manufacturer,
						n2.memorytypeid,
						n2.type,
						n0.memory,
						n0.class,
						n1.speed
					FROM `k_memory` as n0 
					LEFT JOIN `k_memoryclass` as n1
					ON n0.class = n1.class
					LEFT JOIN `k_memorytype` as n2
					ON n0.type = n2.memorytypeid
					ORDER BY n0.serial_number ASC");
		$objs = array();
		while ($u0 = $kd->read()){
			$obj['serial_number'] = $u0[0];
			$obj['manufacturer'] = $u0[1];
			$obj['typeid'] = $u0[2];
			$obj['type'] = $u0[3];
			$obj['memory'] = $u0[4];
			$obj['class'] = $u0[5];
			$obj['speed'] = $u0[6];
			$objs[] = $obj;
		}
		unset($kd);
		print_r($objs);
		//die(print_r($objs));
		return $objs;
	}	
}
?>