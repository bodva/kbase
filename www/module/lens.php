<?php
class klensmounttype{
	var $lensmountid;
	var $type;

	static function save($saveid=-1){
		$savepc = new klensmounttype;
		if ($saveid != -1) {$savepc->load($saveid);}
		$savepc->lensmountid = ($_REQUEST['lensmountid'] != -1) ? ($_REQUEST['lensmountid']) : (0);
		$savepc->manufacturer = $_REQUEST['manufacturer'];
		$savepc->type = $_REQUEST['type'];
		
		if ($saveid != -1){
			$savepc->update();
		} else {
			$savepc->add();
		}

		return $savepc->lensmountid;	
	}

	static function delete ($id) {
		$kd = new kdb;
		$kd->query("DELETE FROM `k_lensmounttype` WHERE `lensmountid`='$id'");
		$kd->query("UPDATE `k_lens` SET `mounttype`='' WHERE `mounttype`='$id'");
		$kd->query("UPDATE `k_phototech` SET `lensmounttype`='' WHERE `lensmounttype`='$id'");
		unset($kd);
	}

	function add(){
		$kd = new kdb;
		$kd->query("INSERT INTO `k_lensmounttype` (
			`type`
		) VALUES (
			'$this->type'
		)
			");
		$this->id = $kd->getlastkey();
		unset($kd);
	}

	function update(){
		$kd = new kdb;
		$kd->query("UPDATE `k_lensmounttype`
		SET
			`type` = '$this->type'
		WHERE
			`lensmountid` = '$this->lensmountid'
		");
		unset($kd);
		
	}

	function isexist($id){
		$result = false;
		$kd = new kdb;
		$kd->query("SELECT `type` FROM `k_lensmounttype` WHERE `lensmountid`='$id'");
		$result = ($u0=$kd->read());
		unset($kd);
		return $result;
	}

	function load($id){
		//echo 'lens mount type load <br>';
		$kd = new kdb;
		$kd->query("SELECT 
			`lensmountid`,
			`type`
			FROM `k_lensmounttype`
				WHERE `lensmountid`=$id");
		if ($u0 = $kd->read()){
			$this->lensmountid = $u0[0];
			$this->type = $u0[1];
		}
	}

	static function getall(){
		$kd = new kdb;
		$kd->query("SELECT 
						n0.lensmountid,
						n0.type
					FROM `k_lensmounttype` as n0
					ORDER BY n0.lensmountid ASC");
		$objs = array();
		while ($u0 = $kd->read()){
			$obj['lensmountid'] = $u0[0];
			$obj['type'] = $u0[1];
			$objs[] = $obj;
		}
		unset($kd);
		return $objs;
	}

}




class klens {
	var $lensid;
	var $manufacturer;
	var $model;
	var $type;
	var $focus;
	var $diaphragm;
	var $mindiaphragm;
	var $mounttype;


	static function save($saveid=-1){
		$savepc = new klens;
		if ($saveid != -1) {$savepc->load($saveid);}

		$savepc->lensid = ($_REQUEST['lensid'] != -1) ? ($_REQUEST['lensid']) : (0);
		$savepc->manufacturer = $_REQUEST['manufacturer'];
		$savepc->model = $_REQUEST['model'];
		$savepc->type = $_REQUEST['type'];
		$savepc->focus = $_REQUEST['focus'];
		$savepc->diaphragm = $_REQUEST['diaphragm'];
		$savepc->mindiaphragm = $_REQUEST['mindiaphragm'];
		$savepc->mounttype = new klensmounttype();
		$savepc->mounttype->load($_REQUEST['mounttype']);
		//die($savepc->mounttype->id);
		
		if ($saveid != -1){
			$savepc->update();
		} else {
			$savepc->add();
		}

		return $savepc->lensid;	
	}

	static function delete ($id) {
		$kd = new kdb;
		$kd->query("DELETE FROM `k_lens` WHERE `lensid`='$id'");
		unset($kd);
	}

	function add(){
		$kd = new kdb;
		//die($this->mounttype->id);
		$mounttype = $this->mounttype->lensmountid;
		$kd->query("INSERT INTO `k_lens` (
			`manufacturer`,
			`model`,
			`type`,
			`focus`,
			`diaphragm`,
			`mindiaphragm`,
			`mounttype`
		) VALUES (
			'$this->manufacturer',
			'$this->model',
			'$this->type',
			'$this->focus',
			'$this->diaphragm',
			'$this->mindiaphragm',
			'$mounttype'
		)
			");
		$this->lensid = $kd->getlastkey();
		unset($kd);
	}

	function update(){
		$kd = new kdb;
		$mounttype = $this->mounttype->lensmountid;
		$kd->query("UPDATE `k_lens`
		SET
			`manufacturer` = '$this->manufacturer',
			`model` = '$this->model',
			`type` = '$this->type',
			`focus` = '$this->focus',
			`diaphragm` = '$this->diaphragm',
			`mindiaphragm` = '$this->mindiaphragm',
			`mounttype` = '$mounttype'
		WHERE
			`lensid` = '$this->lensid'
		");
		unset($kd);
		
	}

	function isexist($id){
		$result = false;
		$kd = new kdb;
		$kd->query("SELECT `model` FROM `k_lens` WHERE `lensid`='$id'");
		$result = ($u0=$kd->read());
		unset($kd);
		return $result;
	}

	function load($id){
		$kd = new kdb;
		$kd->query("SELECT 
			`lensid`,
			`manufacturer`,
			`model`,
			`type`,
			`focus`,
			`diaphragm`,
			`mindiaphragm`,
			`mounttype`
			FROM `k_lens`
				WHERE `lensid`=$id");
		if ($u0 = $kd->read()){
			$this->lensid = $u0[0];
			$this->manufacturer = $u0[1];
			$this->model = $u0[2];
			$this->type = $u0[3];
			$this->focus = $u0[4];
			$this->diaphragm = $u0[5];
			$this->mindiaphragm = $u0[6];
			$this->mounttype = new klensmounttype();
			$this->mounttype->load($u0[7]);

		}
	}

	static function getall(){
		$kd = new kdb;
		$kd->query("SELECT 
						n0.lensid,
						n0.manufacturer,
						n0.model,
						n0.type,
						n0.focus,
						n0.diaphragm,
						n0.mindiaphragm,
						n0.mounttype,
						n1.type
					FROM `k_lens` as n0 
					LEFT JOIN `k_lensmounttype` as n1
					ON n1.lensmountid = n0.mounttype
					ORDER BY n0.lensid ASC");
		$objs = array();
		while ($u0 = $kd->read()){
			$obj['lensid'] = $u0[0];
			$obj['manufacturer'] = $u0[1];
			$obj['model'] = $u0[2];
			$obj['type'] = $u0[3];
			$obj['focus'] = $u0[4];
			$obj['diaphragm'] = $u0[5];
			$obj['mindiaphragm'] = $u0[6];
			$obj['mounttypeid'] = $u0[7];
			$obj['mounttype'] = $u0[8];
			$objs[] = $obj;
		}
		unset($kd);
		//die(print_r($objs));
		return $objs;
	}
}
?>