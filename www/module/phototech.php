<?php
class kphototech {
	var $id;
	var $manufacturer;
	var $model;
	var $resolution;
	var $opticalzoom;
	var $display;
	var $memorytype;
	var $matrix;
	var $baterytype;
	var $lensmounttype;


	static function save($saveid=-1){
		//echo 'phototech save <br>';
		$savepc = new kphototech;
		if ($saveid != -1) {$savepc->load($saveid);}
		$savepc->id = ($_REQUEST['id'] != -1) ? ($_REQUEST['id']) : (0);
		$savepc->manufacturer = $_REQUEST['manufacturer'];
		$savepc->model = $_REQUEST['model'];
		$savepc->resolution = $_REQUEST['resolution'];
		$savepc->opticalzoom = $_REQUEST['opticalzoom'];
		$savepc->display = $_REQUEST['display'];

		$savepc->memorytype = new kmemorytype();
		$savepc->memorytype->load($_REQUEST['memorytype']);

		$savepc->matrix = $_REQUEST['matrix'];
		$savepc->baterytype = $_REQUEST['baterytype'];

		$savepc->lensmounttype = new klensmounttype();
		$savepc->lensmounttype->load($_REQUEST['lensmounttype']);
		
		if ($saveid != -1){
			$savepc->update();
		} else {
			$savepc->add();
		}

		return $savepc->id;	
	}

	function delete ($id) {
		$kd = new kdb;
		$kd->query("DELETE FROM `k_phototech` WHERE `phototechid`='$id'");
		unset($kd);
	}

	function add(){
		$kd = new kdb;
		$memorytype = $this->memorytype->memorytypeid;
		$lensmounttype = $this->lensmounttype->lensmountid;
		$kd->query("INSERT INTO `k_phototech` (
			`manufacturer`,
			`model`,
			`resolution`,
			`opticalzoom`,
			`display`,
			`memorytype`,
			`matrix`,
			`baterytype`,
			`lensmounttype`
		) VALUES (
			'$this->manufacturer',
			'$this->model',
			'$this->resolution',
			'$this->opticalzoom',
			'$this->display',
			'$memorytype',
			'$this->matrix',
			'$this->baterytype',
			'$lensmounttype'
		)
			");
		$this->id = $kd->getlastkey();
		unset($kd);
	}

	function update(){
		//echo 'phototech update <br>';
		$kd = new kdb;
		$memorytype = $this->memorytype->memorytypeid;
		$lensmounttype = $this->lensmounttype->lensmountid;
		$kd->query("UPDATE `k_phototech`
		SET
			`phototechid` = '$this->id',
			`manufacturer` = '$this->manufacturer',
			`model` = '$this->model',
			`resolution` = '$this->resolution',
			`opticalzoom` = '$this->opticalzoom',
			`display` = '$this->display',
			`memorytype` = '$memorytype',
			`matrix` = '$this->matrix',
			`baterytype` = '$this->baterytype',
			`lensmounttype` = '$lensmounttype'
		");
		unset($kd);
		
	}

	function isexist($id){
		$result = false;
		$kd = new kdb;
		$kd->query("SELECT `phototechid` FROM `k_phototech` WHERE `phototechid`='$id'");
		$result = ($u0=$kd->read());
		unset($kd);
		return $result;
	}

	function load($id){
		//echo 'phototech load <br>';
		$kd = new kdb;
		$kd->query("SELECT 
			`phototechid`,
			`manufacturer`,
			`model`,
			`resolution`,
			`opticalzoom`,
			`display`,
			`memorytype`,
			`matrix`,
			`baterytype`,
			`lensmounttype`
			FROM `k_phototech`
				WHERE `phototechid`=$id");
		if ($u0 = $kd->read()){
			$this->id = $u0[0];
			$this->manufacturer = $u0[1];
			$this->model = $u0[2];
			$this->resolution = $u0[3];
			$this->opticalzoom = $u0[4];
			$this->display = $u0[5];

			$this->memorytype = new kmemorytype();
			$this->memorytype->load($u0[6]);

			$this->matrix = $u0[7];
			$this->baterytype = $u0[8];
			$this->lensmounttype = new klensmounttype();
			$this->lensmounttype->load($u0[9]);
		}
	}
	
	static function getall(){
		$kd = new kdb;
		$kd->query("SELECT 
						n0.phototechid,
						n0.manufacturer,
						n0.model,
						n0.resolution,
						n0.opticalzoom ,
						n0.display ,
						n1.memorytypeid,
						n1.type,
						n0.matrix,
						n0.baterytype,
						n2.type, 
						n2.lensmountid 
					FROM `k_phototech` as n0 
					LEFT JOIN `k_memorytype` as n1
					ON n0.memorytype = n1.memorytypeid
					LEFT JOIN `k_lensmounttype` as n2
					ON n0.lensmounttype = n2.lensmountid
					ORDER BY n0.phototechid ASC");
		$objs = array();
		while ($u0 = $kd->read()){
			$obj['id'] = $u0[0];
			$obj['manufacturer'] = $u0[1];
			$obj['model'] = $u0[2];
			$obj['resolution'] = $u0[3];
			$obj['opticalzoom'] = $u0[4];
			$obj['display'] = $u0[5];
			$obj['memorytypeid'] = $u0[6];
			$obj['memorytype'] = $u0[7];
			$obj['matrix'] = $u0[8];
			$obj['baterytype'] = $u0[9];
			$obj['lensmounttype'] = $u0[10];
			$obj['lensmounttypeid'] = $u0[11];
			$objs[] = $obj;
		}
		unset($kd);
		return $objs;
	}
}
?>