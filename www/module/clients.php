<?php
class kclients {
	var $id;
	var $fio;
	var $postindex;
	var $adress;


	static function save($saveid=-1){
		$savepc = new kclients;
		if ($saveid != -1) {$savepc->load($saveid);}
		$savepc->id = ($_REQUEST['clientid'] != -1) ? ($_REQUEST['clientid']) : (0);
		$savepc->fio = $_REQUEST['fio'];
		$savepc->postindex = $_REQUEST['postindex'];
		$savepc->adress = $_REQUEST['adress'];
		
		if ($saveid != -1){
			$savepc->update();
		} else {
			$savepc->add();
		}

		return $savepc->id;	
	}

	static function delete ($id) {
		$kd = new kdb;
		$kd->query("DELETE FROM `k_clients` WHERE `clientid`='$id'");
		unset($kd);
	}

	function add(){
		$kd = new kdb;
		$query = "INSERT INTO `k_clients` (
			`fio`,
			`postindex`,
			`adress`
		) VALUES (
			'$this->fio',
			'$this->postindex',
			'$this->adress'
		)";
		if (kconfig::$dbtype == 'pgsql'){ $query .= " RETURNING clientid"; }
		$kd->query($query);
		$this->id = $kd->getlastkey();
		unset($kd);
	}

	function update(){
		$kd = new kdb;
		$kd->query("UPDATE `k_clients`
		SET
			`fio` = '$this->fio',
			`postindex` = '$this->postindex',
			`adress` = '$this->adress'
		WHERE
			`clientid` = '$this->id'
		");
		unset($kd);
		
	}

	function isexist($id){
		$result = false;
		$kd = new kdb;
		$kd->query("SELECT `clientid` FROM `k_clients` WHERE `clientid`='$id'");
		$result = ($u0=$kd->read());
		unset($kd);
		return $result;
	}

	function load($id){
		$kd = new kdb;
		$kd->query("SELECT 
			`clientid`,
			`fio`,
			`postindex`,
			`adress`
			FROM `k_clients`
				WHERE `clientid`=$id");
		if ($u0 = $kd->read()){
			$this->id = $u0[0];
			$this->fio = $u0[1];
			$this->postindex = $u0[2];
			$this->adress = $u0[3];
		}
	}
}
?>