<?php
class ktripop {
	var $tripopid;
	var $manufacturer;
	var $model;

	static function save($saveid=-1){
		$savepc = new ktripop;
		if ($saveid != -1) {$savepc->load($saveid);}
		$savepc->tripopid = ($_REQUEST['tripopid'] != -1) ? ($_REQUEST['tripopid']) : (0);
		//die($savepc->id);
		$savepc->manufacturer = $_REQUEST['manufacturer'];
		$savepc->model = $_REQUEST['model'];
		
		if ($saveid != -1){
			$savepc->update();
		} else {
			$savepc->add();
		}

		return $savepc->tripopid;	
	}

	static function delete ($id) {
		$kd = new kdb;
		$kd->query("DELETE FROM `k_tripop` WHERE `tripopid`='$id'");
		unset($kd);
	}

	function add(){
		$kd = new kdb;
		$kd->query("INSERT INTO `k_tripop` (
			`manufacturer`,
			`model`
		) VALUES (
			'$this->manufacturer',
			'$this->model'
		)
			");
		$this->id = $kd->getlastkey();
		unset($kd);
	}

	function update(){
		$kd = new kdb;
		$kd->query("UPDATE `k_tripop`
		SET
			`manufacturer` = '$this->manufacturer',
			`model` = '$this->model'
		WHERE
			`tripopid` = '$this->tripopid'
		");
		unset($kd);
		
	}

	function isexist($id){
		$result = false;
		$kd = new kdb;
		$kd->query("SELECT `tripopid` FROM `k_tripop` WHERE `tripopid`='$id'");
		$result = ($u0=$kd->read());
		unset($kd);
		return $result;
	}

	function load($id){
		$kd = new kdb;
		$kd->query("SELECT 
			`tripopid`,
			`manufacturer`,
			`model`
			FROM `k_tripop`
				WHERE `tripopid`=$id");
		if ($u0 = $kd->read()){
			$this->tripopid = $u0[0];
			$this->manufacturer = $u0[1];
			$this->model = $u0[2];
		}
	}

	static function getall(){
		$kd = new kdb;
		$kd->query("SELECT `tripopid`,`manufacturer`,`model` FROM `k_tripop` ORDER BY `tripopid` ASC");
		$objs = array();
		while ($u0 = $kd->read()){
			$obj['tripopid'] = $u0[0];
			$obj['manufacturer'] = $u0[1];
			$obj['model'] = $u0[2];
			$objs[] = $obj;
		}
		unset($kd);
		return $objs;
	}
}
?>