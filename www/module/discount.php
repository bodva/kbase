<?php
class kdiscounttype {
	var $type;
	var $discount;


	static function save($saveid=''){
		$savepc = new kdiscounttype;
		//if ($saveid != -1) {$savepc->load($saveid);}
		if (self::isexist($saveid)) {$savepc->load($saveid);}
		//die($saveid);
		$savepc->type = $_REQUEST['type'];
		$savepc->discount = $_REQUEST['discount'];
		
		if(self::isexist($saveid)){
			$savepc->update();
		} else {
			$savepc->add();
		}

		return $savepc->id;	
	}

	static function delete ($id) {
		$kd = new kdb;
		$kd->query("DELETE FROM `k_discounttype` WHERE `type`='$id'");
		unset($kd);
	}

	function add(){
		$kd = new kdb;
		$kd->query("INSERT INTO `k_discounttype` (
			`type`,
			`discount`
		) VALUES (
			'$this->type',
			'$this->discount'
		)
			");
		$this->id = $kd->getlastkey();
		unset($kd);
	}

	function update(){
		$kd = new kdb;
		$kd->query("UPDATE `k_discounttype`
		SET
			`discount` = '$this->discount'
		WHERE
			`type` = '$this->type'
		");
		unset($kd);
		
	}

	static function isexist($id){
		$result = false;
		$kd = new kdb;
		$kd->query("SELECT `type` FROM `k_discounttype` WHERE `type`='$id'");
		$arr = $kd->read();
		$result = !empty($arr);
		unset($kd);
		return $result;
	}

	function load($id){
		$kd = new kdb;
		$kd->query("SELECT 
			`type`,
			`discount`
			FROM `k_discounttype`
				WHERE `type`='$id'");
		if ($u0 = $kd->read()){
			$this->type = $u0[0];
			$this->discount = $u0[1];
		}
	}

	static function getall(){
		$kd = new kdb;
		$kd->query("SELECT 
						n0.type,
						n0.discount
					FROM `k_discounttype` as n0
					ORDER BY n0.type ASC");
		$objs = array();
		while ($u0 = $kd->read()){
			$obj['type'] = $u0[0];
			$obj['discount'] = $u0[1];
			$objs[] = $obj;
		}
		unset($kd);
		return $objs;
	}
}







class kdiscount {
	var $discountid;
	var $clientid;
	var $type;
	var $sum;


	static function save($saveid=-1){
		//echo 'discountid save <br>';
		$savepc = new kdiscount;
		if ($saveid != -1) {$savepc->load($saveid);}

		$savepc->sum = $_REQUEST['sum'];
		$savepc->discountid = ($_REQUEST['discountid'] != -1) ? ($_REQUEST['discountid']) : (0);
		$savepc->clientid = new kclients();
		$savepc->clientid->load($_REQUEST['clientid']);

		$savepc->type = new kdiscounttype();
		$savepc->type->load($_REQUEST['type']);
		
		if ($saveid != -1){
			$savepc->update();
		} else {
			$savepc->add();
		}

		return $savepc->discontid;	
	}

	static function delete ($id) {
		$kd = new kdb;
		$kd->query("DELETE FROM `k_discount` WHERE `discountid`='$id'");
		unset($kd);
	}

	function add(){
		//kclients::save(-1);
		$kd = new kdb;
		$clientid = kclients::save(-1);//$this->clientid->id;
		$type = $this->type->type;
		$kd->query("INSERT INTO `k_discount` (
			`clientid`,
			`type`,
			`sum`
		) VALUES (
			'$clientid',
			'$type',
			'$this->sum'
		)
			");
		$this->discountid = $kd->getlastkey();
		unset($kd);
	}

	function update(){
		$kd = new kdb;
		$type = $this->type->type;
		//die($type);
		$clientid = $this->clientid->id;
		kclients::save($clientid);
		$kd->query("UPDATE `k_discount`
		SET
			`clientid` = '$clientid',
			`type` = '$type',
			`sum` = '$this->sum'
		WHERE
			`discountid` = '$this->discountid'
		");
		unset($kd);
		
	}

	function isexist($id){
		$result = false;
		$kd = new kdb;
		$kd->query("SELECT `discountid` FROM `k_discount` WHERE `discountid`='$id'");
		$result = ($u0=$kd->read());
		unset($kd);
		return $result;
	}

	function load($id){
		$kd = new kdb;
		$kd->query("SELECT 
			`discountid`,
			`clientid`,
			`type`,
			`sum`
			FROM `k_discount`
				WHERE `discountid`=$id");
		if ($u0 = $kd->read()){
			$this->discontid = $u0[0];

			$this->clientid = new kclients();
			$this->clientid->load($u0[1]);

			$this->type = new kdiscounttype();
			$this->type->load($u0[2]);

			$this->sum = $u0[3];
		}
	}

	static function getall(){
		$kd = new kdb;
		$kd->query("SELECT 
						n0.discountid,
						n0.type,
						n2.discount,
						n0.sum,
						n0.clientid,
						n1.fio,
						n1.postindex,
						n1.adress
					FROM `k_discount` as n0 
					LEFT JOIN `k_clients` as n1
					ON n0.clientid = n1.clientid
					LEFT JOIN `k_discounttype` as n2
					ON n0.type = n2.type
					ORDER BY n0.discountid ASC");
		$objs = array();
		while ($u0 = $kd->read()){
			$obj['discountid'] = $u0[0];
			$obj['type'] = $u0[1];
			$obj['discount'] = $u0[2];
			$obj['sum'] = $u0[3];
			$obj['clientid'] = $u0[4];
			$obj['fio'] = $u0[5];
			$obj['postindex'] = $u0[6];
			$obj['adress'] = $u0[7];
			$objs[] = $obj;
		}
		unset($kd);
		return $objs;
	}	
}
?>