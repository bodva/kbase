<?php
class kflash {
	var $flashid;
	var $manufacturer;
	var $model;

	static function save($saveid=-1){
		$savepc = new kflash;
		if ($saveid != -1) {$savepc->load($saveid);}
		$savepc->flashid = ($_REQUEST['flashid'] != -1) ? ($_REQUEST['flashid']) : ("0");
		$savepc->manufacturer = $_REQUEST['manufacturer'];
		$savepc->model = $_REQUEST['model'];
		
		if ($saveid != -1){
			$savepc->update();
		} else {
			$savepc->add();
		}

		return $savepc->flashid;	
	}

	static function delete ($id) {
		$kd = new kdb;
		$kd->query("DELETE FROM `k_flash` WHERE `flashid`='$id'");
		unset($kd);
	}

	function add(){
		$kd = new kdb;
		$kd->query("INSERT INTO `k_flash` (
			`manufacturer`,
			`model`
		) VALUES (
			'$this->manufacturer',
			'$this->model'
		)
			");
		$this->id = $kd->getlastkey();
		unset($kd);
	}

	function update(){
		$kd = new kdb;
		$kd->query("UPDATE `k_flash`
		SET
			`manufacturer` = '$this->manufacturer',
			`model` = '$this->model'
		WHERE
			`flashid` = '$this->flashid'
		");
		unset($kd);
		
	}

	function isexist($id){
		$result = false;
		$kd = new kdb;
		$kd->query("SELECT `flashid` FROM `k_flash` WHERE `flashid`='$id'");
		$result = ($u0=$kd->read());
		unset($kd);
		return $result;
	}

	function load($id){
		$kd = new kdb;
		$kd->query("SELECT 
			`flashid`,
			`manufacturer`,
			`model`
			FROM `k_flash`
				WHERE `flashid`=$id");
		if ($u0 = $kd->read()){
			$this->id = $u0[0];
			$this->manufacturer = $u0[1];
			$this->model = $u0[2];
		}
	}

	static function getall(){
		$kd = new kdb;
		$kd->query("SELECT `flashid`,`manufacturer`,`model` FROM `k_flash` ORDER BY `flashid` ASC");
		$objs = array();
		while ($u0 = $kd->read()){
			$obj['flashid'] = $u0[0];
			$obj['manufacturer'] = $u0[1];
			$obj['model'] = $u0[2];
			$objs[] = $obj;
		}
		unset($kd);
		return $objs;
	}
}
?>