﻿
--
-- Dumping data for table k_clients
--



-- --------------------------------------------------------

--
-- Table structure for table k_discount
--

CREATE TABLE IF NOT EXISTS k_discount (
  discontid SERIAL,
  clientid integer,
  type text,
  sum integer,
  CONSTRAINT discontid PRIMARY KEY (discontid)
)  ;

--
-- Dumping data for table k_discount
--



-- --------------------------------------------------------

--
-- Table structure for table k_discounttype
--

CREATE TABLE IF NOT EXISTS k_discounttype (
  type text,
  discount integer,
  CONSTRAINT type PRIMARY KEY (type)
) ;

--
-- Dumping data for table k_discounttype
--



-- --------------------------------------------------------

--
-- Table structure for table k_flash
--

CREATE TABLE IF NOT EXISTS k_flash (
  flashid SERIAL,
  manufacturer text,
  model text,
  CONSTRAINT flashid PRIMARY KEY (flashid)
)  ;

--
-- Dumping data for table k_flash
--



-- --------------------------------------------------------

--
-- Table structure for table k_lens
--

CREATE TABLE IF NOT EXISTS k_lens (
  lensid SERIAL,
  manufacturer text,
  model text,
  type text,
  focus text,
  diaphragm text,
  mindiaphragm text,
  mounttype integer,
  CONSTRAINT lensid PRIMARY KEY (lensid)
)  ;

--
-- Dumping data for table k_lens
--



-- --------------------------------------------------------

--
-- Table structure for table k_lensmounttype
--

CREATE TABLE IF NOT EXISTS k_lensmounttype (
  lensmountid SERIAL,
  type text NOT NULL,
  CONSTRAINT lensmountid PRIMARY KEY (lensmountid)
)  ;

--
-- Dumping data for table k_lensmounttype
--



-- --------------------------------------------------------

--
-- Table structure for table k_memory
--

CREATE TABLE IF NOT EXISTS k_memory (
  serial_number SERIAL,
  manufacturer text,
  type integer,
  memory integer,
  class integer,
  CONSTRAINT serial_number PRIMARY KEY (serial_number),
  CONSTRAINT serial_number UNIQUE (serial_number)
)  ;

--
-- Dumping data for table k_memory
--


-- --------------------------------------------------------

--
-- Table structure for table k_memoryclass
--

CREATE TABLE IF NOT EXISTS k_memoryclass (
  "class" SERIAL,
  speed integer,
  CONSTRAINT "class" PRIMARY KEY ("class")
)  ;

--
-- Dumping data for table k_memoryclass
--

-- --------------------------------------------------------

--
-- Table structure for table k_memorytype
--

CREATE TABLE IF NOT EXISTS k_memorytype (
  memorytypeid SERIAL,
  type text,
  CONSTRAINT memorytypeid PRIMARY KEY (memorytypeid)
) ;

--
-- Dumping data for table k_memorytype
--


-- --------------------------------------------------------

--
-- Table structure for table k_phototech
--

CREATE TABLE IF NOT EXISTS k_phototech (
  phototechid SERIAL,
  manufacturer text,
  model text,
  resolution text,
  opticalzoom text,
  display text,
  memorytype integer,
  matrix text,
  baterytype text,
  lensmounttype integer,
  CONSTRAINT phototechid PRIMARY KEY (phototechid)
  );

--
-- Dumping data for table k_phototech
--


-- --------------------------------------------------------

--
-- Table structure for table k_tripop
--

CREATE TABLE IF NOT EXISTS k_tripop (
  tripopid SERIAL,
  manufacturer text,
  model text,
  CONSTRAINT tripopid PRIMARY KEY (tripopid)
)  ;

--
-- Dumping data for table k_tripop
--


-- --------------------------------------------------------

--
-- Table structure for table k_users
--

CREATE TABLE IF NOT EXISTS k_users (
  userid SERIAL,
  "user" text,
  passwd text,
  isadmin integer,
  issaler integer,
  CONSTRAINT userid PRIMARY KEY (userid),
  CONSTRAINT "user" UNIQUE ("user")
)  ;

--
-- Dumping data for table k_users
--

INSERT INTO k_users (userid, "user", passwd, isadmin, issaler) VALUES
(1, 'BoDVa', '1', 0, 1),
(9, 'admin', 'admin1', 1, 1),
(12, 'saler', 'saler1', 0, 1),
(14, 'user', 'user1', 0, 0),
(15, 'lala', 'lala1', 0, 1);
