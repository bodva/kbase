-- phpMyAdmin SQL Dump
-- version 3.2.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 07, 2011 at 07:14 PM
-- Server version: 5.1.40
-- PHP Version: 5.2.12

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `kphotomag`
--
CREATE DATABASE `kphotomag` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `kphotomag`;

-- --------------------------------------------------------

--
-- Table structure for table `k_clients`
--

CREATE TABLE IF NOT EXISTS `k_clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fio` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `postindex` int(11) NOT NULL,
  `adress` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `k_clients`
--


-- --------------------------------------------------------

--
-- Table structure for table `k_discount`
--

CREATE TABLE IF NOT EXISTS `k_discount` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientid` int(11) NOT NULL,
  `type` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `sum` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `k_discount`
--


-- --------------------------------------------------------

--
-- Table structure for table `k_discounttype`
--

CREATE TABLE IF NOT EXISTS `k_discounttype` (
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `discount` int(11) NOT NULL,
  PRIMARY KEY (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `k_discounttype`
--


-- --------------------------------------------------------

--
-- Table structure for table `k_flash`
--

CREATE TABLE IF NOT EXISTS `k_flash` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manufacturer` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `model` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `k_flash`
--


-- --------------------------------------------------------

--
-- Table structure for table `k_lens`
--

CREATE TABLE IF NOT EXISTS `k_lens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manufacturer` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `model` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `type` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `focus` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `diaphragm` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `mindiaphragm` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `mounttype` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `k_lens`
--


-- --------------------------------------------------------

--
-- Table structure for table `k_lensmounttype`
--

CREATE TABLE IF NOT EXISTS `k_lensmounttype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `k_lensmounttype`
--


-- --------------------------------------------------------

--
-- Table structure for table `k_memory`
--

CREATE TABLE IF NOT EXISTS `k_memory` (
  `serial_number` int(11) NOT NULL,
  `manufacturer` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `type` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `memory` int(11) NOT NULL,
  `class` int(11) NOT NULL,
  PRIMARY KEY (`serial_number`),
  UNIQUE KEY `serial_number` (`serial_number`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `k_memory`
--


-- --------------------------------------------------------

--
-- Table structure for table `k_memoryclass`
--

CREATE TABLE IF NOT EXISTS `k_memoryclass` (
  `class` int(11) NOT NULL,
  `speed` int(11) NOT NULL,
  PRIMARY KEY (`class`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `k_memoryclass`
--


-- --------------------------------------------------------

--
-- Table structure for table `k_phototech`
--

CREATE TABLE IF NOT EXISTS `k_phototech` (
  `manufacturer` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `model` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `resolution` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `opticalzoom` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `display` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `memorytype` int(11) NOT NULL,
  `matrix` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `baterytype` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `lensmounttype` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `k_phototech`
--


-- --------------------------------------------------------

--
-- Table structure for table `k_tripop`
--

CREATE TABLE IF NOT EXISTS `k_tripop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manufacturer` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `model` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `k_tripop`
--


-- --------------------------------------------------------

--
-- Table structure for table `k_users`
--

CREATE TABLE IF NOT EXISTS `k_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `passwd` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `isadmin` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `k_users`
--

INSERT INTO `k_users` (`id`, `user`, `passwd`, `isadmin`) VALUES
(1, 'BoDVa', '1', 1);
